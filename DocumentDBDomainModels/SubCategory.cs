﻿namespace DocumentDBDomainModels
{
    public class SubCategory
    {
        public int SubCategoryId { get; set; }
        public string SubCategoryName { get; set; }
    }
}
