﻿using System;
using System.Collections.Generic;

namespace DocumentDBDomainModels
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public IList<SubCategory> SubCategories { get; set; }
        public Guid Id { get; set; }
    }

}

