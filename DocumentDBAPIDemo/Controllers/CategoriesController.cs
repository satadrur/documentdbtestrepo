﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using AutoMapper.Configuration;
using DocumentDBDomainModels;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;

namespace DocumentDBAPIDemo.Controllers
{
    public class CategoriesController : ApiController
    {
        public string EndPoint { get; set; }

        public string Masterkey { get; set; }

        public CategoriesController()
        {
            EndPoint = ConfigurationManager.AppSettings["DocumentDbEndPoint"];
            Masterkey = ConfigurationManager.AppSettings["DocumentDbMasterKey"];
        }

        public IEnumerable<Category> Get()
        {
            using (var client = new DocumentClient(new Uri(EndPoint), Masterkey))
            {
                var databases = GetDatabases(client);

                var database = databases.First();

                var collections = GetCollections(client, database);

                var collection = collections.FirstOrDefault(x => x.Id.Equals("categories", StringComparison.OrdinalIgnoreCase));

                var documents = GetCategories(client, collection, database);

                Mapper.Initialize(new MapperConfigurationExpression());

                return documents.Select(x => Mapper.Map(x, new Category())).ToList();
            }
        }

        private IEnumerable<Document> GetCategories(DocumentClient client, DocumentCollection collection, Database database)
        {
            var categories =
                client.CreateDocumentQuery(UriFactory.CreateDocumentCollectionUri(database.Id,
                    collection.Id)).ToList();

            return categories;
        }

        [Route("api/Databases")]
        public IEnumerable<Database> GetDatabases()
        {

            using (var client = new DocumentClient(new Uri(EndPoint), Masterkey))
            {
                var databases = GetDatabases(client);

                return databases;
            }
        }


        [Route("api/Databases")]
        [HttpPost]
        public Database CreateDatabase(string id)
        {
            using (var client = new DocumentClient(new Uri(EndPoint), Masterkey))
            {
                var database = CreateDatabase(client, id);

                return database;
            }
        }

        [Route("api/Collections")]
        public IEnumerable<DocumentCollection> GetCollections()
        {

            using (var client = new DocumentClient(new Uri(EndPoint), Masterkey))
            {
                var databases = GetDatabases(client);

                var database = databases.First();

                var collections = GetCollections(client, database);

                return collections;
            }
        }

        [Route("api/Collections")]
        [HttpPost]
        public DocumentCollection CreateCollection(string id)
        {
            using (var client = new DocumentClient(new Uri(EndPoint), Masterkey))
            {
                var databases = GetDatabases(client);

                var database = databases.First();

                var collection = CreateCollection(client, id, database);

                return collection;
            }
        }

        private DocumentCollection CreateCollection(DocumentClient client, string id, Database database)
        {
            var collection = client.CreateDocumentCollectionAsync(UriFactory.CreateDatabaseUri(database.Id),
                new DocumentCollection { Id = id }, new RequestOptions { OfferType = "S1" });

            return collection.Result;
        }

        [Route("api/Documents")]
        public IEnumerable<Document> GetDocuments()
        {

            using (var client = new DocumentClient(new Uri(EndPoint), Masterkey))
            {
                var databases = GetDatabases(client);

                var database = databases.First();

                var collections = GetCollections(client, database);

                var collection = collections.First();

                var documents = GetDocuments(client, collection, database);

                return documents;
            }
        }

        [HttpPost]
        public async Task<Category> CreateCategory(Category category)
        {
            using (var client = new DocumentClient(new Uri(EndPoint), Masterkey))
            {
                var databases = GetDatabases(client);

                var database = databases.First();

                var collections = GetCollections(client, database);

                var collection = collections.FirstOrDefault(x => x.Id.Equals("categories", StringComparison.OrdinalIgnoreCase));

                var document = await CreateDocument(client, collection, database, category);

                Mapper.Initialize(new MapperConfigurationExpression());
                return Mapper.Map(document, new Category());
                
            }

        }

        private async Task<Document> CreateDocument(DocumentClient client, DocumentCollection collection, Database database, Category category)
        {
            var categoryCreated = await client.CreateDocumentAsync(
                UriFactory.CreateDocumentCollectionUri(database.Id, collection.Id), category);


            return categoryCreated;
        }

        private IEnumerable<Document> GetDocuments(DocumentClient client, DocumentCollection collection, Database database)
        {
            var documents = client.CreateDocumentQuery(UriFactory.CreateDocumentCollectionUri(database.Id, collection.Id));

            return documents.ToList();
        }

        private IEnumerable<Database> GetDatabases(DocumentClient client)
        {
            var databases = client.CreateDatabaseQuery().ToList();

            return databases;
        }

        private Database CreateDatabase(DocumentClient client, string id)
        {
            var database = client.CreateDatabaseAsync(new Database { Id = id });

            return database.Result.Resource;
        }

        private static IEnumerable<DocumentCollection> GetCollections(IDocumentClient client, Resource database)
        {
            var collections = client.CreateDocumentCollectionQuery(database.SelfLink).ToList();

            return collections;

        }
    }
}